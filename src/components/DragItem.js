import { Draggable } from "react-beautiful-dnd";
import { Grid, Paper, Typography, IconButton } from "@material-ui/core";
import EditIcon from '@material-ui/icons/Edit';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';

const getItemStyle = (isDragging, draggableStyle) => ({
    userSelect: "none",
    background: isDragging && "lightblue",
    display: 'flex', padding: 10,
    ...draggableStyle
});

const DragItem = ({ todo, index, handleEdit, handleRemove }) => {
    const { id, content, status } = todo;

    return (
        <Draggable draggableId={`${id}`} index={index}>
            {(provided, snapshot) => (
                <Paper variant="outlined"
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    style={getItemStyle(
                        snapshot.isDragging,
                        provided.draggableProps.style
                    )}>
                    <Grid container
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >
                        <Grid item xs={8}>
                            <Typography
                                style={{
                                    textDecoration: status === 'completed' ? "line-through" : "none",
                                    fontStyle: status === 'completed' ? 'italic' : 'normal',
                                    cursor: 'pointer',
                                    color: status === 'inprogress' ? 'green' : status === 'completed' ? 'lightgrey' : '#000',
                                    fontWeight: (status === 'incomplete' || status === 'inprogress') && 'bolder'
                                }}
                            >
                                {content}
                            </Typography>
                        </Grid>
                        <Grid item xs={2}>
                            <IconButton onClick={handleEdit}><EditIcon /></IconButton>
                        </Grid>
                        <Grid item xs={2}>
                            <IconButton onClick={handleRemove}><DeleteForeverIcon /></IconButton>
                        </Grid>
                    </Grid>
                </Paper>
            )}
        </Draggable>
    )
}

export default DragItem




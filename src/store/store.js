import { createStore, persist } from 'easy-peasy';

import todoModel from './models'

const store = createStore(
    {
        todos: persist(todoModel),

    },
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store
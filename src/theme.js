import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    typography: {
        fontFamily: "Raleway",
    },
    palette: {
        default: {
            main: "#6e7c88",
        },
    },
    props: {
        MuiButtonBase: {
            disableRipple: true
        },
        MuiCheckbox: {
            disableRipple: true
        }
    },
    overrides: {
        MuiButton: {
            root: {
                padding: "9px 40px",
                borderRadius: 0,
                letterSpacing: 0.75,
                fontWeight: 600,
                "&:hover": {
                    backgroundColor: "transparent"
                },
            },
            text: {
                color: "#000",
                borderColor: "#000"
            },
            outlined: {
                color: "#555",
                borderColor: "#777"
            },
            contained: {
                color: "#444",
                padding: "15px 40px",
                letterSpacing: 2,
                "&:hover": {
                    boxShadow: "none"
                },
                "&:active": {
                    color: "#fff",
                    backgroundColor: "#000",
                    boxShadow: "none"
                },
            }
        },
        MuiTypography: {
            h1: {
                color: "#fff",
                fontSize: 128,
                letterSpacing: 64,
                fontWeight: "bolder",
                textShadow: '16px 0px 1px rgba(51, 153, 255)',
            },
            h2: {
                color: "#666",
                fontSize: 20,
            },
            h3: {
                color: "#535353",
                fontWeight: 800,
                fontSize: 20
            },
        },
    },
});


export default theme;
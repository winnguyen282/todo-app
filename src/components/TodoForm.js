import { useState } from "react";
import { useStoreActions } from 'easy-peasy';
import { Paper, InputBase, Divider, IconButton } from "@material-ui/core";
import { v4 as uuidv4 } from 'uuid';
import AddIcon from '@material-ui/icons/Add';



const TodoForm = () => {
    const addTodo = useStoreActions((actions) => actions.todos.addTodo);

    const [content, setContent] = useState("");

    const handleSubmit = e => {
        e.preventDefault();
        if (content.trim() === "") {
        } else {
            const uuid = uuidv4();
            const id = uuid.slice(0, 8);
            addTodo({
                id,
                content,
                status: 'incomplete',
            });
        }
        setContent("");
    };

    return (
        <>
            <Paper
                component="form"
                style={{
                    padding: '2px 4px',
                    display: 'flex',
                    borderRadius: 24
                }}
                onSubmit={handleSubmit}>
                <InputBase
                    value={content}
                    style={{ marginLeft: 24, flex: 1 }}
                    placeholder="What do you want to do?"
                    inputProps={{ 'aria-label': 'add' }}
                    onChange={(e) => setContent(e.target.value)}
                />
                <Divider style={{ height: 40, margin: 4 }} orientation="vertical" />
                <IconButton type="submit" color="primary" aria-label="directions">
                    <AddIcon />
                </IconButton>
            </Paper>
        </>
    )
}

export default TodoForm


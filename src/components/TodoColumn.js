import { Droppable } from "react-beautiful-dnd";
import { Paper, Grid, Typography, Box } from '@material-ui/core';

import Todo from './Todo'

const getListStyle = isDraggingOver => ({
    background: isDraggingOver && "moccasin",
});

const TodoColumn = (props) => {
    const { data, index, name } = props

    //    console.log('props: ', props)

    return (
        <Grid item xs={12} md={4} key={index}>
            <Paper style={{ padding: 32, borderRadius: 16 }}>
                <Typography variant="h3" style={{ paddingBottom: 24 }}>
                    {name} ({data.length})
                </Typography>
                <Droppable droppableId={`${index}`} index={index}>
                    {(provided, snapshot) => (
                        <Box
                            ref={provided.innerRef}
                            style={getListStyle(snapshot.isDraggingOver)}
                            {...provided.droppableProps}
                        >
                            {data?.length === 0
                                ? (<Typography style={{ paddingTop: 16 }}>No item</Typography>)
                                : (data?.map((todo, index) => (
                                    <Todo
                                        key={todo.id}
                                        todo={todo}
                                        index={index}
                                    />
                                )))}
                            {provided.placeholder}
                        </Box>
                    )}
                </Droppable>
            </Paper>
        </Grid>
    )
}

export default TodoColumn
import { useStoreState, useStoreActions } from 'easy-peasy';
import { DragDropContext } from "react-beautiful-dnd";
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography, Box } from '@material-ui/core';
import _cloneDeep from 'lodash/cloneDeep'
import _flattenDeep from 'lodash/flattenDeep'

import TodoForm from "./TodoForm";
import TodoColumn from "./TodoColumn";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        textAlign: 'center'
    },
    mobile: {
        [theme.breakpoints.down('sm')]: {
            fontSize: 64,
        }
    }
}));

const TodoList = () => {
    const classes = useStyles();
    const todosComputed = useStoreState((state) => state.todos.todosComputed);
    const moveTodo = useStoreActions((actions) => actions.todos.moveTodo);
    const updateTodos = useStoreActions((actions) => actions.todos.updateTodos);

    const reorder = (list, startIndex, endestinationIndexex) => {
        const result = Array.from(list);
        const [removed] = result.splice(startIndex, 1);
        result.splice(endestinationIndexex, 0, removed);
        return result;
    };

    const move = (source, destination, droppableSource, droppableDestination) => {
        const sourceClone = Array.from(source);
        const destClone = Array.from(destination);
        const [removed] = sourceClone.splice(droppableSource.index, 1);
        const dropId = droppableDestination.droppableId
        let updatedStatus = 'incomplete'
        dropId === '1'
            ? updatedStatus = 'inprogress'
            : dropId === '2'
                ? updatedStatus = 'completed'
                : updatedStatus = 'incomplete'
        const dropItem = { ...removed, status: updatedStatus };
        moveTodo(dropItem)

        destClone.splice(droppableDestination.index, 0, dropItem);
        const result = [];
        result[droppableSource.droppableId] = sourceClone;
        result[droppableDestination.droppableId] = destClone;
        return result;
    };

    const onDragEnd = (result) => {
        const { source, destination } = result;
        if (!destination) return;
        const sourceIndex = +source.droppableId;
        const destinationIndex = +destination.droppableId;
        if (sourceIndex === destinationIndex) {
            const items = reorder(todosComputed[sourceIndex], source.index, destination.index);
            const newState = _cloneDeep(todosComputed);
            newState[sourceIndex] = items;
            updateTodos(_flattenDeep(newState));
        } else {
            const result = move(todosComputed[sourceIndex], todosComputed[destinationIndex], source, destination);
            const newState = _cloneDeep(todosComputed);
            newState[sourceIndex] = result[sourceIndex];
            newState[destinationIndex] = result[destinationIndex];
            updateTodos(_flattenDeep(newState));
        }
    }

    return (
        <>
            <Box p={4} m={4} textAlign="center">
                <Typography className={classes.mobile} variant="h1">Todo</Typography>
            </Box>
            <DragDropContext onDragEnd={onDragEnd}>
                <Box className={classes.root}>
                    <Grid
                        container spacing={3}
                        direction="row"
                        justify="center"
                        alignItems="flex-start"
                    >
                        <Grid item xs={9}>
                            <TodoForm />
                        </Grid>
                        {todosComputed.map((data, index) => {
                            const name = index === 0 ? 'Incomplete' : index === 1 ? "In Progress" : "Completed"
                            return (<TodoColumn key={index.toString()} data={data} index={index} name={name} />)
                        })}
                    </Grid>
                </Box>
            </ DragDropContext>
        </>
    )
}

export default TodoList

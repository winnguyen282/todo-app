import React from 'react'
import { Container } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/core/styles';
import { StoreProvider } from 'easy-peasy';

import TodoList from './components/TodoList'
import theme from './theme'
import store from "./store/store";

function App() {
  return (
    <StoreProvider store={store}>
      <ThemeProvider theme={theme}>
        <Container>
          <TodoList />
        </Container>
      </ThemeProvider>
    </StoreProvider>
  );
}

export default App;

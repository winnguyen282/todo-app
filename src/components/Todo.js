import { useState } from "react";
import { useStoreActions } from 'easy-peasy';
import { Grid, Paper, TextField, IconButton } from "@material-ui/core";
import CloseIcon from '@material-ui/icons/Close';
import CheckIcon from '@material-ui/icons/Check';

import DragItem from "./DragItem";


const Todo = ({ todo, index }) => {
    const { id, content } = todo;
    const editTodo = useStoreActions((actions) => actions.todos.editTodo);
    const removeTodo = useStoreActions((actions) => actions.todos.removeTodo);
    const [value, newValue] = useState(todo)
    const [isEditing, setIsEditing] = useState(false);

    return (
        <>
            {isEditing
                ? <Paper variant="outlined" style={{ display: 'flex', padding: 10 }}>
                    <Grid container
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >
                        <Grid item xs={8}>
                            <TextField
                                defaultValue={content}
                                onChange={(e) => {
                                    newValue({
                                        ...value,
                                        content: e.target.value
                                    })
                                }
                                }
                                fullWidth
                                id="task"
                                type="text"
                            />
                        </Grid>
                        <Grid item xs={2}>
                            <IconButton variant="outlined" type="submit" onClick={async () => {
                                await editTodo(value)
                                setIsEditing(false)
                            }}>
                                <CheckIcon color="primary" /></IconButton>
                        </Grid>
                        <Grid item xs={2}>
                            <IconButton onClick={() => setIsEditing(false)}><CloseIcon color="secondary" /></IconButton>
                        </Grid>
                    </Grid>
                </Paper>
                : <DragItem todo={todo} index={index} handleEdit={() => setIsEditing(true)} handleRemove={() => removeTodo(id)} />
            }
        </>
    )
}

export default Todo




import { action, computed } from "easy-peasy";

const todoModel = {
    todos: [
        { id: '1', content: 'Example 1', status: 'incomplete' },
        { id: '2', content: 'Example 2', status: 'incomplete' },
        { id: '3', content: 'Example 3', status: 'incomplete' },
        { id: '4', content: 'Example 4', status: 'incomplete' },
    ],

    todosComputed: computed((state) => {
        const incompleteTodo = state.todos.filter(todo => todo.status === 'incomplete')
        const inprogressTodo = state.todos.filter(todo => todo.status === 'inprogress')
        const completedTodo = state.todos.filter(todo => todo.status === 'completed')
        return [incompleteTodo, inprogressTodo, completedTodo]
    }),

    addTodo: action((state, payload) => {
        state.todos.push(payload);
    }),

    editTodo: action((state, payload) => {
        state.todos.map(todo => {
            return todo.id === payload.id ? (todo.content = payload.content) : todo.content
        })
    }),

    removeTodo: action((state, id) => {
        state.todos = state.todos.filter(todo => todo.id !== id)
    }),

    moveTodo: action((state, payload) => {
        state.todos.map(todo => {
            return todo.id === payload.id ? (todo.status = payload.status) : todo.status
        })
    }),

    updateTodos: action((state, payload) => {
        state.todos = [...payload]
    })
}


export default todoModel;